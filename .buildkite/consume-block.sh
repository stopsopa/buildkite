echo "release-name:$(buildkite-agent meta-data get "release-name")"
echo "release-stream:$(buildkite-agent meta-data get "release-stream")"

pwd
ls -la

_SUCCESS="$(buildkite-agent meta-data get "success")"

# https://buildkite.com/docs/agent/v3/cli-annotate#annotation-styles
# https://commonmark.org/help/

if [ "$_SUCCESS" = "false" ]; then

  echo "exit 1 - directive from 'block' step"

  buildkite-agent annotate 'release failed [https://google.com?q=fail](https://google.com?q=fail)' --style 'error' --context 'ctx-error'

  exit 1
fi

buildkite-agent annotate 'release success [https://google.com?q=success](https://google.com?q=success)' --style 'success' --context 'ctx-success'