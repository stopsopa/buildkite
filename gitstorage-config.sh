#!/bin/bash

# used in
# git@bitbucket.org:phaseii/geocoder.git

GITSTORAGESOURCE="git@bitbucket.org:phaseii/doc.git"

GITSTORAGETARGETDIR="pii-geocoder"

GITSTORAGELIST=(
    ".env::$GITSTORAGETARGETDIR/.env"
    ".env.kub.test::$GITSTORAGETARGETDIR/.env.kub.test"
    "gitstorage-config.sh::$GITSTORAGETARGETDIR/gitstorage-config.sh"
)

